#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_STUDENTS 100
#define MAX_NAME_LENGTH 51
#define DATE_LENGTH 11
#define REGISTRATION_LENGTH 7
#define MAX_PROGRAM_CODE_LENGTH 5

struct Student {
    char name[MAX_NAME_LENGTH];
    char date_of_birth[DATE_LENGTH];
    char registration_number[REGISTRATION_LENGTH];
    char program_code[MAX_PROGRAM_CODE_LENGTH];
    float annual_tuition;
};

struct Student students[MAX_STUDENTS];
int num_students = 0;

void print_menu() {
    printf("\nMenu:\n");
    printf("1. Add a student\n");
    printf("2. View all students\n");
    printf("3. Update a student\n");
    printf("4. Delete a student\n");
    printf("5. Search for a student by registration number\n");
    printf("6. Sort students\n");
    printf("7. Export records to CSV\n");
    printf("8. Exit\n");
}

void add_student() {
    if (num_students >= MAX_STUDENTS) {
        printf("Cannot add more students. Maximum limit reached.\n");
        return;
    }

    printf("Enter student details\n");

    printf("Name: ");
    fgets(students[num_students].name, MAX_NAME_LENGTH, stdin);
    students[num_students].name[strcspn(students[num_students].name, "\n")] = '\0'; // Remove trailing newline

    printf("Date of Birth (YYYY-MM-DD): ");
    fgets(students[num_students].date_of_birth, DATE_LENGTH, stdin);
    students[num_students].date_of_birth[strcspn(students[num_students].date_of_birth, "\n")] = '\0'; // Remove trailing newline

    getchar();
   
    printf("Registration Number: ");
    fgets(students[num_students].registration_number, REGISTRATION_LENGTH, stdin);
    students[num_students].registration_number[strcspn(students[num_students].registration_number, "\n")] = '\0'; // Remove trailing newline


    printf("\nProgram Code: "); // New prompt for program code
    fgets(students[num_students].program_code, MAX_PROGRAM_CODE_LENGTH, stdin);
    students[num_students].program_code[strcspn(students[num_students].program_code, "\n")] = '\0'; // Remove trailing newline

    printf("Annual Tuition: ");
    scanf("%f", &students[num_students].annual_tuition);
    getchar(); // Consume newline character

    num_students++;

    printf("Student added successfully.\n");
}


void view_students() {
    if (num_students == 0) {
        printf("No students found.\n");
        return;
    }
    
    printf("List of students:\n");
    for (int i = 0; i < num_students; i++) {
        printf("Student %d:\n", i + 1);
        printf("Name: %s\n", students[i].name);
        printf("Date of Birth: %s\n", students[i].date_of_birth);
        printf("Registration Number: %s\n", students[i].registration_number);
        printf("Program Code: %s\n", students[i].program_code);
        printf("Annual Tuition: $%.2f\n", students[i].annual_tuition);
        printf("\n");
    }
}

void update_student() {
    if (num_students == 0) {
        printf("No students found.\n");
        return;
    }

    char registration[REGISTRATION_LENGTH];
    printf("Enter the registration number of the student to update: ");
    fgets(registration, REGISTRATION_LENGTH, stdin);
    registration[strcspn(registration, "\n")] = '\0'; // Remove trailing newline

    int found = 0;
    for (int i = 0; i < num_students; i++) {
        if (strcmp(students[i].registration_number, registration) == 0) {
            printf("Enter new details for the student:\n");

            printf("Name: ");
            fgets(students[i].name, MAX_NAME_LENGTH, stdin);
            students[i].name[strcspn(students[i].name, "\n")] = '\0'; // Remove trailing newline

            printf("Date of Birth (YYYY-MM-DD): ");
            fgets(students[i].date_of_birth, DATE_LENGTH, stdin);
            students[i].date_of_birth[strcspn(students[i].date_of_birth, "\n")] = '\0'; // Remove trailing newline

            printf("Registration Number: ");
            fgets(students[i].registration_number, REGISTRATION_LENGTH, stdin);
            students[i].registration_number[strcspn(students[i].registration_number, "\n")] = '\0'; // Remove trailing newline

            printf("Program Code: ");
            fgets(students[i].program_code, MAX_PROGRAM_CODE_LENGTH, stdin);
            students[i].program_code[strcspn(students[i].program_code, "\n")] = '\0'; // Remove trailing newline

            printf("Annual Tuition: ");
            scanf("%f", &students[i].annual_tuition);
            getchar(); // Consume newline character

            printf("Student details updated successfully.\n");
            found = 1;
            break;
        }
    }

    if (!found) {
        printf("Student with registration number %s not found.\n", registration);
    }
}


void delete_student() {
    if (num_students == 0) {
        printf("No students found.\n");
        return;
    }

    char registration[REGISTRATION_LENGTH];
    printf("Enter the registration number of the student to delete: ");
    fgets(registration, REGISTRATION_LENGTH, stdin);
    registration[strcspn(registration, "\n")] = '\0'; // Remove trailing newline

    int found = 0;
    for (int i = 0; i < num_students; i++) {
        if (strcmp(students[i].registration_number, registration) == 0) {
            // Shift elements to left to overwrite the deleted student
            for (int j = i; j < num_students - 1; j++) {
                strcpy(students[j].name, students[j + 1].name);
                strcpy(students[j].date_of_birth, students[j + 1].date_of_birth);
                strcpy(students[j].registration_number, students[j + 1].registration_number);
                strcpy(students[j].program_code, students[j + 1].program_code);
                students[j].annual_tuition = students[j + 1].annual_tuition;
            }
            num_students--;
            printf("Student deleted successfully.\n");
            found = 1;
            break;
        }
    }

    if (!found) {
        printf("Student with registration number %s not found.\n", registration);
    }
}


void search_student_by_registration() {
    if (num_students == 0) {
        printf("No students found.\n");
        return;
    }

    char registration[REGISTRATION_LENGTH];
    printf("Enter the registration number of the student to search: ");
    fgets(registration, REGISTRATION_LENGTH, stdin);
    registration[strcspn(registration, "\n")] = '\0'; // Remove trailing newline

    int found = 0;
    for (int i = 0; i < num_students; i++) {
        if (strcmp(students[i].registration_number, registration) == 0) {
            printf("Student found:\n");
            printf("Name: %s\n", students[i].name);
            printf("Date of Birth: %s\n", students[i].date_of_birth);
            printf("Registration Number: %s\n", students[i].registration_number);
            printf("Program Code: %s\n", students[i].program_code);
            printf("Annual Tuition: Shs.%.0f\n", students[i].annual_tuition);
            found = 1;
            break;
        }
    }

    if (!found) {
        printf("Student with registration number %s not found.\n", registration);
    }
}

int compare_by_name(const void *a, const void *b) {
    return strcmp(((struct Student*)a)->name, ((struct Student*)b)->name);
}

int compare_by_registration(const void *a, const void *b) {
    return strcmp(((struct Student*)a)->registration_number, ((struct Student*)b)->registration_number);
}


void sort_students(int sort_option) {
    if (sort_option == 1) {
        qsort(students, num_students, sizeof(struct Student), compare_by_name);
        printf("Students sorted by name.\n");
    } else if (sort_option == 2) {
        qsort(students, num_students, sizeof(struct Student), compare_by_registration);
        printf("Students sorted by registration number.\n");
    } else {
        printf("Invalid sorting option.\n");
    }
}


void export_to_csv() {
    FILE *fp;
    fp = fopen("student_records.csv", "w"); // "w" mode overwrites the file
    
    if (fp == NULL) {
        printf("Error opening file for writing.\n");
        return;
    }
    // Write headings
    fprintf(fp, "Name,Date of Birth,Registration Number,Program Code,Annual Tuition\n");
    
    for (int i = 0; i < num_students; i++) {
        fprintf(fp, "%s,%s,%s,%s,%.2f\n", students[i].name, students[i].date_of_birth, students[i].registration_number, students[i].program_code, students[i].annual_tuition);
    }
    
    fclose(fp);
    printf("Records exported to CSV successfully.\n");
}



int main() {
    int choice;
    
    do {
        print_menu();
        printf("Enter your choice: ");
        scanf("%d", &choice);
        getchar(); // consume newline character
        
        switch (choice) {
            case 1:
                add_student();
                break;
            case 2:
                view_students();
                break;
            case 3:
                update_student();
                break;
            case 4:
                delete_student();
                break;
            case 5:
                search_student_by_registration();
                break;
            case 6: {
                int sort_option;
                printf("Select sorting option:\n");
                printf("1. Sort by name\n");
                printf("2. Sort by registration number\n");
                printf("Enter your choice: ");
                scanf("%d", &sort_option);
                getchar(); // consume newline character
                sort_students(sort_option);
                break;
            }
            case 7:
                export_to_csv();
                break;
            case 8:
                printf("Exiting program.\n");
                break;
            default:
                printf("Invalid choice. Please enter a number between 1 and 8.\n");
                break;
        }
    } while (choice != 8);
      
    
    return 0;
}
