##  STUDENT DATABASE SYSTEM
This platform allows the School Admin to uniformly manage students records.

## Features
  STUDENT RECORDS
Manage student information such as;
-   Student name
-   Date of birth
-   Registration number
-   Student program code associated with course
-   Student annual tuition

## User Functionality
 User should be able to input their;
  - Name
  - Date of birth
  - Registration number
  - Program code
  - Annual tuition.

## Data structure used
 Array

## Why we choose an array
  
- simplicity and efficiency for fixed-size datasets
  They have limitations such as fixed size

- Direct Access: 
  Arrays allow direct access to elements using index values. This means you can quickly retrieve or modify elements based on their position in the array

- Sequential Storage:
  Array elements are stored in contiguous memory locations.

- Efficient Memory Usage:
   Arrays have a fixed size determined at compile time, which leads to efficient memory allocation 

- Simple Implementation:
  Arrays are relatively simple to implement and understand. They provide a basic data structure for storing collections of elements


## Links to youtube vedios

- https://youtu.be/iQDNLdxlwNU (Among Tracy Angela 2300723871 23/U/23871/EVE)
- https://youtu.be/6bWQKMorr_g (KANYESIGYE NATHAN 2300709008 23/U/09008/EVE)
- https://youtu.be/ZNHKr42XJlU?si=xin2CIdNoqrFEmc3   KISIRINYA SIRAJJE 2300724072   23/U/24072/EVE
- https://youtu.be/RrmWi-k-fjA?si=MAp_ATE4iagXzyKG (WAMALA ABDUL JUNIOR- 23/U/18475/EVE)





